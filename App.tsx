/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React from 'react';
import {NativeRouter, Route} from 'react-router-native';
import {DefaultTheme, Provider as PaperProvider} from 'react-native-paper';
import routes from './src/routes';
import {setI18nConfig} from './utils/localize';

const theme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: '#AA4336',
    accent: '#AA4336',
  },
};

const App = () => {
  setI18nConfig();

  return (
    <PaperProvider theme={theme}>
      <NativeRouter>
        {routes.map((route, idx) => {
          // TODO: loadable
          return (
            <Route
              key={idx}
              path={route.path}
              exact
              component={route.component}
            />
          );
        })}
      </NativeRouter>
    </PaperProvider>
  );
};

export default App;
