package com.uminekomangareader;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;

import com.davemorrissey.labs.subscaleview.ImageSource;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;

import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.uimanager.SimpleViewManager;
import com.facebook.react.uimanager.ThemedReactContext;
import com.facebook.react.uimanager.annotations.ReactProp;

import java.util.Map;
import java.util.HashMap;


public class ScaleImageViewManager extends SimpleViewManager<SubsamplingScaleImageView> {
    public static final String REACT_CLASS = "ScaleImageView";

    @Override
    public String getName() {
        return REACT_CLASS;
    }

    @Override
    protected SubsamplingScaleImageView createViewInstance(ThemedReactContext reactContext) {
        return new SubsamplingScaleImageView(reactContext);
    }


    @ReactProp(name = "uri")
    public void setUri(SubsamplingScaleImageView view, @Nullable String uri) {
        view.setImage(ImageSource.uri(uri));
    }
}
