package com.uminekomangareader;

import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import java.util.Map;
import java.util.HashMap;

public class AssetsModule extends ReactContextBaseJavaModule {
    AssetsModule(ReactApplicationContext context) {
        super(context);

        // This line creates the obb directory if it doesn't exist
        context.getObbDir();
    }

    @Override
    public String getName() {
        return "AssetsModule";
    }
}
