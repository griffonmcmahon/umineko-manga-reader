Version 20210223
* Fix regression of image quality on zoom on Android

Version 20210213
* Add support for Umineko Murasaki
* Bug fixed on pinch-to-zoom to prevent swiping while zooming
* UI improvements to the reader
* Add immersive mode (fullscreen) to the reader
* Move Installation instructions to a separate page
* Move Credits to a separate About page
* Show app version in About page
* Move Integrity button to More info button
* Add episode version in More info dialog

Version 20210205
* User experience improvements

Version 20210204
* Add support for Chinese simplified and traditional (thanks to Amber)
* Add support for title translations
* Add support for translation credits
* Fix an issue where music apps would scan all Umineko sounds, due to being in Android/media/
* All packs need to be redownloaded (old version is incompatible) and unzipped in Android/obb/com.uminekomangareader/

Version 20210201b
* Create Android/media/com.uminekomangareader/ folder on first start if it doesn't exist.

Version 20210201
* Add support for (upcoming) episode 8
* Move assets path from Android/data/com.uminekomangareader/ to Android/media/com.uminekomangareader/ to workaround Android 11 restrictions. You will need to move your existing files to the new location when upgrading, whether you are running Android 11 or not.
* Add credits section

Version 20210131
* Add ability to turn off BGM/SE/voice

Version 20210128
* Integrity check

Version 20210127
* Make the installation instructions easier to understand
* Remove APK download link from GitLab as it is wrongly detected as ZIP file

Version 20210126
- Huge performance improvements
- New icon
- Fix resuming audio after going to background

Version 1.0
- Initial version
