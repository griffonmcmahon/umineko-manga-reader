import {Link, useHistory} from 'react-router-native';
import {Button, Dialog, Portal} from 'react-native-paper';
import React, {useEffect, useState} from 'react';
import RNFS from 'react-native-fs';
import {translate} from '../../utils/localize';
import {ASSETS_FILE_PATH, ASSETS_RELATIVE_PATH} from '../constants';
import {StyleSheet, Text} from 'react-native';
import {capitalize} from 'lodash';

const s = StyleSheet.create({
  bold: {
    fontWeight: 'bold',
  },
  error: {
    backgroundColor: '#fdd',
    borderRadius: 2,
    padding: 5,
    marginVertical: 2,
  },
  bottomMargin: {
    marginBottom: 10,
  },
});

const EpisodeLink = ({number}) => {
  const history = useHistory();
  const [script, setScript] = useState(null);

  const [visible, setVisible] = useState(false);
  const showDialog = () => setVisible(true);
  const hideDialog = () => setVisible(false);

  useEffect(() => {
    RNFS.readFile(ASSETS_FILE_PATH + 'ep-' + number + '/script.json')
      .then((result) => {
        const jsonScript = JSON.parse(result);
        if ('version' in jsonScript) {
          setScript(jsonScript);
        }
      })
      .catch((err) => {
        console.log('Episode ' + number + ' not available, not showing');
      });
  }, [number]);

  return script ? (
    <Link
      to={'/episode-' + number}
      component={Button}
      mode="contained"
      style={s.bottomMargin}>
      {number >= 1 && number <= 8 ? translate('episode') + ' ' : ''}
      {number} | {script.title}
    </Link>
  ) : (
    <>
      <Portal>
        <Dialog visible={visible} onDismiss={hideDialog}>
          <Dialog.Title>
            {(number >= 1 && number <= 8 ? translate('episode') + ' ' : '') +
              capitalize(number) +
              ' ' +
              translate('unavailable')}
          </Dialog.Title>
          <Dialog.Content>
            <Text style={s.bottomMargin}>{translate('firstTime')}</Text>
            <Button
              onPress={() => history.push('/installation')}
              style={s.bottomMargin}
              mode="outlined">
              {translate('installationInstructions')}
            </Button>
            <Text>
              {translate('checkLocation')}
              <Text style={s.bold}>
                {ASSETS_RELATIVE_PATH + 'ep-' + number + '/'}
              </Text>
              {translate('notNestedFolder')}
            </Text>
          </Dialog.Content>
          <Dialog.Actions>
            <Button onPress={hideDialog}>{translate('understood')}</Button>
          </Dialog.Actions>
        </Dialog>
      </Portal>
      <Button
        mode="contained"
        style={s.bottomMargin}
        color={'#CCC'}
        onPress={showDialog}>
        {number >= 1 && number <= 8 ? translate('episode') + ' ' : ''}
        {number}
      </Button>
    </>
  );
};

export default EpisodeLink;
