import React, {useEffect, useRef, useState} from 'react';
import {
  AppState,
  BackHandler,
  Dimensions,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import {Player} from '@react-native-community/audio-toolkit';
import {translate} from '../../utils/localize';
import {Carousel} from 'react-native-snap-carousel';
import {Button, Dialog, Switch, Portal, Appbar} from 'react-native-paper';
import {Immersive} from 'react-native-immersive';
import ImageViewer from './ImageViewer';
import {useHistory} from 'react-router-native';

const s = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#000',
  },
  bar: {
    position: 'absolute',
    left: 0,
    right: 0,
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  barText: {
    fontWeight: 'bold',
    color: '#FFF',
  },
  topBar: {
    elevation: 0,
    top: 0,
  },
  bottomBar: {
    padding: 10,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    bottom: 0,
  },
});

const {width: viewportWidth, height: viewportHeight} = Dimensions.get('window');

const Reader = ({pagerItems, episode, initialIndex, goBack}) => {
  /** SETTINGS **/
  const [visible, setVisible] = useState(false);
  const showDialog = () => setVisible(true);
  const hideDialog = () => setVisible(false);

  const bgmEnabledRef = useRef(true);
  const [bgmEnabled, setBgmEnabled] = useState(bgmEnabledRef.current);
  const seEnabledRef = useRef(true);
  const [seEnabled, setSeEnabled] = useState(seEnabledRef.current);
  const voiceEnabledRef = useRef(true);
  const [voiceEnabled, setVoiceEnabled] = useState(voiceEnabledRef.current);

  /** GALLERY **/
  const galleryRef = useRef(null);
  const [galleryIndex, setGalleryIndex] = useState(initialIndex);

  const [headerShown, setHeaderShown] = useState(true);

  function toggleHeaderShown() {
    if (headerShown) {
      Immersive.on();
    } else {
      Immersive.off();
    }
    setHeaderShown((currValue) => !currValue);
  }

  /*function hide() {
    setHeaderShown(false);
  }*/

  /** GO BACK **/
  const history = useHistory();

  const goToEpisodeScreen = () => {
    history.push('/episode-' + episode);
    Immersive.off();
    return true;
  };

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', goToEpisodeScreen);
    return () => {
      BackHandler.removeEventListener('hardwareBackPress', goToEpisodeScreen);
    };
  }, []);

  /** AUDIOS **/
  const bgmRef = useRef(null);
  const [bgm, setBgm] = useState(bgmRef.current);
  const seRef = useRef([]);
  const [se, setSe] = useState(seRef.current);
  const voiceRef = useRef(null);
  const [voice, setVoice] = useState(voiceRef.current);

  const loadBgm = (index) => {
    if (pagerItems[index].bgm) {
      // Avoid reloading BGM from start if it is already playing
      if (
        !(
          bgmRef &&
          bgmRef.current &&
          bgmRef.current.path &&
          bgmRef.current.path === pagerItems[index].bgm
        )
      ) {
        let tmpBgm = new Player(pagerItems[index].bgm);
        tmpBgm.looping = true;
        tmpBgm.volume = 0.8;
        tmpBgm.play();
        if (bgmRef.current) {
          bgmRef.current.destroy();
        }
        bgmRef.current = tmpBgm;
      }
    } else {
      if (bgmRef.current) {
        bgmRef.current.destroy();
      }
      bgmRef.current = null;
    }
    setBgm(bgmRef.current);
  };

  const loadSe = (index) => {
    if (seRef.current && Array.isArray(seRef.current) && seRef.current.length) {
      if (pagerItems[index].se && pagerItems[index].se.length) {
        // Remove SE not playing anymore on the new page
        for (let currentPlaySe of seRef.current) {
          let found = false;
          for (let currentPageSe of pagerItems[index].se) {
            if (currentPlaySe.path === currentPageSe) {
              found = true;
            }
          }
          if (!found) {
            currentPlaySe.destroy();
            seRef.current.splice(seRef.current.indexOf(currentPlaySe), 1);
          }
        }

        // Add SE that were not playing before
        for (let currentPageSe of pagerItems[index].se) {
          let found = false;
          for (let currentPlaySe of seRef.current) {
            if (currentPlaySe.path === currentPageSe) {
              found = true;
            }
          }
          if (!found) {
            let tmpSe = new Player(currentPageSe);
            tmpSe.looping = true;
            tmpSe.volume = 0.8;
            tmpSe.play();
            seRef.current.push(tmpSe);
          }
        }
      } else {
        // Removing all playing SE
        for (let currentPlaySe of seRef.current) {
          currentPlaySe.destroy();
          seRef.current.splice(seRef.current.indexOf(currentPlaySe), 1);
        }
      }
    } else {
      seRef.current = pagerItems[index].se.map((seAudio) => {
        let tmpSe = new Player(seAudio);
        tmpSe.looping = true;
        tmpSe.volume = 0.8;
        tmpSe.play();
        return tmpSe;
      });
    }
    setSe(seRef.current);
  };

  const loadVoice = (index) => {
    if (pagerItems[index].voice) {
      let tmpVoice = new Player(pagerItems[index].voice);
      tmpVoice.looping = false;
      tmpVoice.play();
      if (voiceRef.current) {
        voiceRef.current.destroy();
      }
      voiceRef.current = tmpVoice;
    } else {
      if (voiceRef.current) {
        voiceRef.current.destroy();
      }
      voiceRef.current = null;
    }
    setVoice(voiceRef.current);
  };

  const destroyBgm = () => {
    if (bgmRef.current) {
      bgmRef.current.destroy();
      bgmRef.current = null;
      setBgm(bgmRef.current);
    }
  };

  const destroySe = () => {
    for (let seAudio of seRef.current) {
      seAudio.destroy();
    }
    seRef.current = [];
    setSe(seRef.current);
  };

  const destroyVoice = () => {
    if (voiceRef.current) {
      voiceRef.current.destroy();
      voiceRef.current = null;
      setVoice(voiceRef.current);
    }
  };

  const loadAudios = (index) => {
    if (bgmEnabledRef.current) {
      loadBgm(index);
    } else {
      destroyBgm();
    }

    if (seEnabledRef.current) {
      loadSe(index);
    } else {
      destroySe();
    }

    if (voiceEnabledRef.current) {
      loadVoice(index);
    } else {
      destroyVoice();
    }
  };

  const destroyAudios = () => {
    destroyBgm();
    destroySe();
    destroyVoice();
  };

  useEffect(() => {
    if (!bgmEnabled) {
      destroyBgm();
    } else {
      if (
        galleryRef &&
        galleryRef.current &&
        'currentIndex' in galleryRef.current
      ) {
        loadBgm(galleryRef.current.currentIndex);
      }
    }
  }, [bgmEnabled]);

  useEffect(() => {
    if (!seEnabled) {
      destroySe();
    } else {
      if (
        galleryRef &&
        galleryRef.current &&
        'currentIndex' in galleryRef.current
      ) {
        loadSe(galleryRef.current.currentIndex);
      }
    }
  }, [seEnabled]);

  useEffect(() => {
    if (!voiceEnabled) {
      destroyVoice();
    } else {
      if (
        galleryRef &&
        galleryRef.current &&
        'currentIndex' in galleryRef.current
      ) {
        loadVoice(galleryRef.current.currentIndex);
      }
    }
  }, [voiceEnabled]);

  const onIndexChange = (nextIndex) => {
    loadAudios(nextIndex);
    setGalleryIndex(nextIndex);
  };

  useEffect(() => {
    loadAudios(initialIndex);
    return () => {
      destroyAudios();
    };
  }, []);

  /** APP STATE **/
  const appState = useRef(AppState.currentState);

  useEffect(() => {
    AppState.addEventListener('change', _handleAppStateChange);

    return () => {
      AppState.removeEventListener('change', _handleAppStateChange);
    };
  }, []);

  const _handleAppStateChange = (nextAppState) => {
    if (
      appState.current.match(/inactive|background/) &&
      nextAppState === 'active'
    ) {
      destroyAudios();
      if (
        galleryRef &&
        galleryRef.current &&
        'currentIndex' in galleryRef.current
      ) {
        loadAudios(galleryRef.current.currentIndex);
      }
    }

    appState.current = nextAppState;
  };

  return (
    <View style={s.container}>
      <Carousel
        ref={galleryRef}
        firstItem={galleryIndex}
        data={pagerItems}
        vertical={false}
        renderItem={({item, index}) => {
          return (
            <ImageViewer onTap={toggleHeaderShown} source={{uri: item.uri}} />
          );
        }}
        itemWidth={viewportWidth}
        sliderWidth={viewportWidth}
        inactiveSlideOpacity={1}
        inactiveSlideScale={1}
        disableIntervalMomentum={true}
        onSnapToItem={(indexSelected) => onIndexChange(indexSelected)}
      />

      <Portal>
        <Dialog visible={visible} onDismiss={hideDialog}>
          <Dialog.Title>{translate('settings')}</Dialog.Title>
          <Dialog.Content>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                paddingVertical: 10,
              }}>
              <Text style={{flex: 2}}>{translate('backgroundMusic')}</Text>
              <Switch
                value={bgmEnabledRef.current}
                onValueChange={() => {
                  bgmEnabledRef.current = !bgmEnabledRef.current;
                  setBgmEnabled(bgmEnabledRef.current);
                }}
              />
            </View>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                paddingVertical: 10,
              }}>
              <Text style={{flex: 2}}>{translate('soundEffects')}</Text>
              <Switch
                value={seEnabledRef.current}
                onValueChange={() => {
                  seEnabledRef.current = !seEnabledRef.current;
                  setSeEnabled(seEnabledRef.current);
                }}
              />
            </View>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                paddingVertical: 10,
              }}>
              <Text style={{flex: 2}}>{translate('voices')}</Text>
              <Switch
                value={voiceEnabledRef.current}
                onValueChange={() => {
                  voiceEnabledRef.current = !voiceEnabledRef.current;
                  setVoiceEnabled(voiceEnabledRef.current);
                }}
              />
            </View>
          </Dialog.Content>
          <Dialog.Actions>
            <Button onPress={hideDialog}>{translate('done')}</Button>
          </Dialog.Actions>
        </Dialog>
      </Portal>
      {headerShown && (
        <>
          <Appbar style={[s.bar, s.topBar]}>
            <Appbar.BackAction onPress={goToEpisodeScreen} />
            <Appbar.Content
              title={
                translate('episode') +
                ' ' +
                episode +
                ' ' +
                translate('chapter') +
                ' ' +
                pagerItems[galleryIndex].chapter
              }
            />
            <Appbar.Action icon="cog-outline" onPress={showDialog} />
          </Appbar>
          <View style={[s.bar, s.bottomBar]}>
            <Text style={s.barText}>
              {translate('page')} {pagerItems[galleryIndex].page}
            </Text>
          </View>
        </>
      )}
    </View>
  );
};

export default Reader;
