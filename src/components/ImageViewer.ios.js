import React, {useRef} from 'react';
import {Dimensions, Image, View} from 'react-native';
import ImageZoom from 'react-native-image-pan-zoom';

const ImageViewer = ({
  source,
  width = Dimensions.get('window').width,
  height = Dimensions.get('window').height,
  onMove,
  onTap,
  ...props
}) => {
  const scaleValue = useRef(1);
  const touchStart = useRef(null);
  return (
    <ImageZoom
      cropWidth={Dimensions.get('window').width}
      cropHeight={Dimensions.get('window').height}
      imageWidth={width}
      imageHeight={height}
      minScale={1}
      {...props}
      onStartShouldSetPanResponder={(e) => {
        return e.nativeEvent.touches.length === 2 || scaleValue.current > 1;
      }}
      onMove={({scale}) => {
        scaleValue.current = scale;
        onMove && onMove({scale});
      }}>
      <View
        style={{width: '100%', height: '100%'}}
        onTouchStart={(evt) => {
          touchStart.current = evt.nativeEvent;
        }}
        onTouchEnd={(evt) => {
          const diffX = touchStart.current.pageX - evt.nativeEvent.pageX;
          const diffY = touchStart.current.pageY - evt.nativeEvent.pageY;

          const moveDistance = Math.sqrt(
            Math.pow(diffX, 2) + Math.pow(diffY, 2),
          );

          if (
            evt.nativeEvent.changedTouches.length === 1 &&
            moveDistance <= 10
          ) {
            onTap();
          }
        }}
        onStartShouldSetResponder={(e) => {
          return e.nativeEvent.touches.length < 2 && scaleValue.current <= 1;
        }}>
        <Image
          source={source}
          resizeMode="contain"
          style={{width: '100%', height: '100%'}}
        />
      </View>
    </ImageZoom>
  );
};

export default ImageViewer;
