'use strict';
import React, {useRef} from 'react';
import {Dimensions, requireNativeComponent} from 'react-native';

const ImageViewer = ({
  source,
  width = Dimensions.get('window').width,
  height = Dimensions.get('window').height,
  onMove,
  onTap,
  ...props
}) => {
  const touchStart = useRef(null);
  return (
    <RCTAndroidScaleImageView
      onTouchStart={(evt) => {
        touchStart.current = evt.nativeEvent;
      }}
      onTouchEnd={(evt) => {
        const diffX = touchStart.current.pageX - evt.nativeEvent.pageX;
        const diffY = touchStart.current.pageY - evt.nativeEvent.pageY;

        const moveDistance = Math.sqrt(Math.pow(diffX, 2) + Math.pow(diffY, 2));

        if (evt.nativeEvent.changedTouches.length === 1 && moveDistance <= 10) {
          onTap();
        }
      }}
      uri={source.uri}
      style={{width: '100%', height: '100%'}}
    />
  );
};

export default ImageViewer;

const RCTAndroidScaleImageView = requireNativeComponent(
  'ScaleImageView',
  ImageViewer,
);
