import React from 'react';
import {SafeAreaView, ScrollView, StyleSheet, View} from 'react-native';
import {Appbar} from 'react-native-paper';

const styles = StyleSheet.create({
  container: {
    padding: 10,
    marginBottom: 60, // FIXME: Check why we need to do this
  },
});

const Layout = ({children, title, goBack, moreInfo}) => {
  return (
    <SafeAreaView>
      <Appbar.Header>
        {goBack && <Appbar.BackAction onPress={goBack} />}
        <Appbar.Content title={title} />
        {moreInfo && (
          <Appbar.Action icon="information-outline" onPress={moreInfo} />
        )}
      </Appbar.Header>
      <ScrollView contentInsetAdjustmentBehavior="automatic">
        <View style={styles.container}>{children}</View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default Layout;
