import {Platform} from 'react-native';

export const IMAGE_TREE = Platform.select({
  android: () => require('./androidTree.jpg'),
  ios: () => require('./iosTree.jpg'),
  default: () => require('./iosTree.jpg'),
})();
