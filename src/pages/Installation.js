﻿import React, {useEffect} from 'react';
import Layout from '../components/Layout';
import {BackHandler, Image, Linking, StyleSheet, Text} from 'react-native';
import {translate} from '../../utils/localize';
import {useHistory} from 'react-router-native';
import {ASSETS_RELATIVE_PATH} from '../constants';
import {IMAGE_TREE} from '../images';

const s = StyleSheet.create({
  center: {
    textAlign: 'center',
  },
  bottomMargin: {
    marginBottom: 10,
  },
  link: {
    color: '#AA4336',
  },
});

const Installation = () => {
  const history = useHistory();

  const goToMainScreen = () => {
    history.push('/');
    return true;
  };

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', goToMainScreen);
    return () => {
      BackHandler.removeEventListener('hardwareBackPress', goToMainScreen);
    };
  }, []);

  return (
    <Layout
      title={translate('installationInstructions')}
      goBack={() => goToMainScreen()}>
      <Text style={s.bottomMargin}>{translate('instructionTip')}</Text>

      <Text>
        {translate('instructionA')}
        <Text
          style={s.link}
          onPress={() =>
            Linking.openURL(
              'https://mega.nz/folder/xDok3YIY#uywP-zp36qCpUxvWdV-17Q',
            )
          }>
          {translate('thisLink')}
        </Text>
        {translate('instructionB')}
        <Text style={s.bold}>common.opus.VERSION.zip</Text>
        {translate('instructionC')}
        <Text style={s.bold}>ep-X.LANG.VERSION.zip</Text>
        {translate('instructionD')}
      </Text>
      <Text>
        <Text style={s.bold}>X</Text>
        {translate('instructionE')}
      </Text>
      <Text>
        <Text style={s.bold}>LANG</Text>
        {translate('instructionF')}
      </Text>
      <Text>
        <Text style={s.bold}>VERSION</Text>
        {translate('instructionG')}
      </Text>
      <Text style={s.bottomMargin}>{translate('instructionH')}</Text>

      <Text>
        {translate('instructionI')}
        <Text style={s.bold}>{ASSETS_RELATIVE_PATH}</Text>
        {translate('instructionJ')}
      </Text>
      <Image style={s.bottomMargin} source={IMAGE_TREE} />

      <Text>{translate('instructionK')}</Text>
    </Layout>
  );
};

export default Installation;
