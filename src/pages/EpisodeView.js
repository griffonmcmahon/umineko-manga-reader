import React, {useEffect, useState} from 'react';
import {Link, useHistory, useParams} from 'react-router-native';
import _, {capitalize} from 'lodash';
import {
  ActivityIndicator,
  Button,
  Dialog,
  Portal,
  Title,
} from 'react-native-paper';
import RNFS from 'react-native-fs';
import {BackHandler, Linking, StyleSheet, Text} from 'react-native';
import Layout from '../components/Layout';
import {translate} from '../../utils/localize';
import {ASSETS_FILE_PATH, ASSETS_RELATIVE_PATH} from '../constants';
import DeviceInfo from 'react-native-device-info';

const s = StyleSheet.create({
  bold: {
    fontWeight: 'bold',
  },
  bottomMargin: {
    marginBottom: 10,
  },
  center: {
    textAlign: 'center',
  },
  link: {
    color: '#AA4336',
  },
  topMargin: {
    marginTop: 10,
  },
  /*cellSuccess: {
    backgroundColor: '#4caf50',
  },
  cellWarning: {
    backgroundColor: '#ff9800',
  },
  cellError: {
    backgroundColor: '#f44336',
  },*/
});

const EpisodeView = () => {
  const history = useHistory();

  const [visible, setVisible] = useState(false);
  const showDialog = () => setVisible(true);
  const hideDialog = () => setVisible(false);

  const goToMainScreen = () => {
    history.push('/');
    return true;
  };

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', goToMainScreen);
    return () => {
      BackHandler.removeEventListener('hardwareBackPress', goToMainScreen);
    };
  }, []);

  const params = useParams();
  const episode = params.episode;

  const [script, setScript] = useState(null);
  const [loadMessage, setLoadMessage] = useState(
    <ActivityIndicator size="large" />,
  );

  useEffect(() => {
    RNFS.readFile(ASSETS_FILE_PATH + 'ep-' + episode + '/script.json')
      .then((result) => {
        setLoadMessage(<ActivityIndicator size="large" />);
        setScript(JSON.parse(result));
      })
      .catch((err) => {
        if (err.code === 'ENOENT') {
          setLoadMessage(
            <>
              <Text style={s.bottomMargin}>
                {translate('episode') +
                  ' ' +
                  episode +
                  ' ' +
                  translate('unavailable')}
                .
              </Text>
              <Text style={s.bottomMargin}>{translate('firstTime')}</Text>
              <Button
                onPress={() => history.push('/installation')}
                style={s.bottomMargin}
                mode="outlined">
                {translate('installationInstructions')}
              </Button>
              <Text style={s.bottomMargin}>
                {translate('checkLocation') +
                  ASSETS_RELATIVE_PATH +
                  'ep-' +
                  episode +
                  '/' +
                  translate('notNestedFolder')}
              </Text>
            </>,
          );
        } else {
          setLoadMessage(translate('errorLoadingScript') + episode);
        }
        setScript(null);
        console.log(err.message, err.code);
      });
  }, [episode]);

  return script ? (
    <Layout
      title={
        (episode >= 1 && episode <= 8 ? translate('episode') + ' ' : '') +
        capitalize(episode)
      }
      goBack={() => goToMainScreen()}
      moreInfo={() => showDialog()}>
      <Title>{script.title}</Title>
      <Text>
        {translate('story')}
        {translate('colon')} Ryūkishi07
      </Text>
      <Text>
        {translate('art')}
        {translate('colon')} {script.art}
      </Text>

      {Array.isArray(script.translations[script.lang]) &&
        script.translations[script.lang].length > 0 && (
          <>
            <Text>
              {translate('translation')}
              {translate('colon')}
            </Text>
            {script.translations[script.lang].map((translator) => (
              <React.Fragment key={'trans' + translator.name}>
                {translator.url !== null ? (
                  <Text
                    style={s.link}
                    onPress={() => Linking.openURL(translator.url)}>
                    {translator.name}
                  </Text>
                ) : (
                  <Text>{translator.name}</Text>
                )}
              </React.Fragment>
            ))}
          </>
        )}

      {!_.isEmpty(script.volumes) &&
        Object.values(script.volumes).map((chapters) =>
          chapters.map((chapter) => (
            <Link
              key={'link-ep-' + episode + '-ch-' + chapter}
              to={'/episode-' + episode + '/chapter-' + chapter}
              component={Button}
              mode="contained"
              style={s.topMargin}>
              {translate('chapter')} {chapter}
              {script.chapters[chapter].title[script.lang] !== null &&
                ' | ' + script.chapters[chapter].title[script.lang]}
            </Link>
          )),
        )}

      <Portal>
        <Dialog visible={visible} onDismiss={hideDialog}>
          <Dialog.Title>
            {(episode >= 1 && episode <= 8 ? translate('episode') + ' ' : '') +
              capitalize(episode) +
              ' ' +
              translate('version') +
              ' ' +
              DeviceInfo.getVersion()}
          </Dialog.Title>
          <Dialog.Content>
            <Button
              onPress={() => history.push('/episode-' + episode + '/integrity')}
              style={s.bottomMargin}
              mode="outlined">
              {translate('checkIntegrity')}
            </Button>
          </Dialog.Content>
          <Dialog.Actions>
            <Button onPress={hideDialog}>{translate('done')}</Button>
          </Dialog.Actions>
        </Dialog>
      </Portal>
      {/*<Title>Avancement</Title>
          <DataTable>
            <DataTable.Header>
              <DataTable.Row>
                <DataTable.Cell>Chapitre</DataTable.Cell>
                <DataTable.Cell>Page</DataTable.Cell>
                <DataTable.Cell>Musique</DataTable.Cell>
                <DataTable.Cell>Effets</DataTable.Cell>
                <DataTable.Cell>Doublage</DataTable.Cell>
              </DataTable.Row>
            </DataTable.Header>
            <DataTable.Body>
              {_.map(script.chapters, (pages, chNumber) => {
                return (
                  <>
                    {_.map(script.chapters[chNumber], (page, pageNumber) => {
                      const failClass =
                        _.isEmpty(page.se) && !page.bgm && !page.voice
                          ? classes.cellError
                          : classes.cellWarning;
                      return (
                        <DataTable.Row>
                          <DataTable.Cell>{chNumber}</DataTable.Cell>
                          <DataTable.Cell>{pageNumber + 1}</DataTable.Cell>
                          <DataTable.Cell
                            className={
                              page.bgm ? classes.cellSuccess : failClass
                            }>
                            {page.bgm}
                          </DataTable.Cell>
                          <DataTable.Cell
                            className={
                              _.isEmpty(page.se)
                                ? failClass
                                : classes.cellSuccess
                            }>
                            {JSON.stringify(page.se)}
                          </DataTable.Cell>
                          <DataTable.Cell
                            className={
                              page.voice ? classes.cellSuccess : failClass
                            }>
                            {page.voice ? 'Oui' : 'Non'}
                          </DataTable.Cell>
                        </DataTable.Row>
                      );
                    })}
                  </>
                );
              })}
            </DataTable.Body>
          </DataTable>*/}
    </Layout>
  ) : (
    <Layout
      title={translate('episode') + ' ' + episode}
      goBack={() => goToMainScreen()}>
      <Text>{loadMessage}</Text>
    </Layout>
  );
};

export default EpisodeView;
