﻿import React, {useEffect} from 'react';
import Layout from '../components/Layout';
import {BackHandler, Linking, StyleSheet, Text} from 'react-native';
import {translate} from '../../utils/localize';
import {useHistory} from 'react-router-native';
import {Title} from 'react-native-paper';
import DeviceInfo from 'react-native-device-info';
import {capitalize} from 'lodash';

const s = StyleSheet.create({
  center: {
    textAlign: 'center',
  },
  marginTop: {
    marginTop: 20,
  },
  link: {
    color: '#AA4336',
  },
});

const About = () => {
  const history = useHistory();

  const goToMainScreen = () => {
    history.push('/');
    return true;
  };

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', goToMainScreen);
    return () => {
      BackHandler.removeEventListener('hardwareBackPress', goToMainScreen);
    };
  }, []);

  return (
    <Layout title={translate('about')} goBack={() => goToMainScreen()}>
      <Title style={s.center}>Umineko Manga Reader</Title>
      <Title style={s.center}>
        {capitalize(translate('version'))} {DeviceInfo.getVersion()}
      </Title>

      <Title style={s.marginTop}>{translate('credits')}</Title>
      <Text>
        <Text
          style={s.link}
          onPress={() => {
            Linking.openURL('https://gitlab.com/papjul/umineko-manga-reader');
          }}>
          Umineko Manga Reader
        </Text>
        {translate('projectAndContributors')}.
      </Text>
      <Text>
        <Text
          style={s.link}
          onPress={() => {
            Linking.openURL('https://github.com/papjul/umineko-reader-assets');
          }}>
          Umineko Reader Assets
        </Text>
        {translate('projectAndContributors')}.
      </Text>
      <Text>07th-expansion & Ryukishi07</Text>
    </Layout>
  );
};

export default About;
