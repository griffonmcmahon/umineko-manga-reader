import React, {useEffect, useState} from 'react';
import {useHistory, useParams} from 'react-router-native';
import {ActivityIndicator, Title} from 'react-native-paper';
import RNFS from 'react-native-fs';
import {BackHandler, Text} from 'react-native';
import Layout from '../components/Layout';
import {translate} from '../../utils/localize';
import _ from 'lodash';
import {ASSETS_FILE_PATH, ASSETS_RELATIVE_PATH} from '../constants';

// TODO: Avoid redundancy
async function checkIntegrity(script, episode) {
  let tmpErrorMessages = [];
  for (const [chapterIndex, chapter] of Object.entries(script.chapters)) {
    for (let i = 0; i < chapter.pages.length; ++i) {
      // Check image
      const fileExists = await RNFS.exists(
        ASSETS_FILE_PATH +
          'ep-' +
          episode +
          '/img/ch-' +
          chapterIndex +
          '/' +
          chapter.pages[i].page +
          '.jpg',
      );
      if (!fileExists) {
        tmpErrorMessages.push(
          translate('missing') +
            ': ' +
            ASSETS_RELATIVE_PATH +
            'ep-' +
            episode +
            '/img/ch-' +
            chapterIndex +
            '/' +
            chapter.pages[i].page +
            '.jpg',
        );
      }

      // Check BGM
      if (chapter.pages[i].bgm) {
        const bgmExists = await RNFS.exists(
          ASSETS_FILE_PATH + 'common/bgm/umib_' + chapter.pages[i].bgm + '.ogg',
        );
        if (!bgmExists) {
          // Do not show the same BGM error twice
          if (
            !tmpErrorMessages.includes(
              translate('missing') +
                ': ' +
                ASSETS_RELATIVE_PATH +
                'common/bgm/umib_' +
                chapter.pages[i].bgm +
                '.ogg',
            )
          ) {
            tmpErrorMessages.push(
              translate('missing') +
                ': ' +
                ASSETS_RELATIVE_PATH +
                'common/bgm/umib_' +
                chapter.pages[i].bgm +
                '.ogg',
            );
          }
        }
      }

      for (const se of chapter.pages[i].se) {
        const seExists = await RNFS.exists(
          ASSETS_FILE_PATH + 'common/se/umilse_' + se + '.ogg',
        );
        if (!seExists) {
          // Do not show the same BGM error twice
          if (
            !tmpErrorMessages.includes(
              translate('missing') +
                ': ' +
                ASSETS_RELATIVE_PATH +
                'common/se/umilse_' +
                se +
                '.ogg',
            )
          ) {
            tmpErrorMessages.push(
              translate('missing') +
                ': ' +
                ASSETS_RELATIVE_PATH +
                'common/se/umilse_' +
                se +
                '.ogg',
            );
          }
        }
      }

      // Check Voice
      if (chapter.pages[i].voice) {
        const voiceExists = await RNFS.exists(
          ASSETS_FILE_PATH +
            'ep-' +
            episode +
            '/voice/ch-' +
            chapterIndex +
            '/' +
            chapter.pages[i].page +
            '.ogg',
        );
        if (!voiceExists) {
          tmpErrorMessages.push(
            translate('missing') +
              ': ' +
              ASSETS_RELATIVE_PATH +
              'ep-' +
              episode +
              '/voice/ch-' +
              chapterIndex +
              '/' +
              chapter.pages[i].page +
              '.ogg',
          );
        }
      }
    }
  }
  return tmpErrorMessages;
}

const EpisodeIntegrityView = () => {
  const history = useHistory();
  const params = useParams();
  const episode = params.episode;

  const goToEpisodeScreen = () => {
    history.push('/episode-' + episode);
    return true;
  };

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', goToEpisodeScreen);
    return () => {
      BackHandler.removeEventListener('hardwareBackPress', goToEpisodeScreen);
    };
  }, []);

  const [loadMessage, setLoadMessage] = useState(
    <ActivityIndicator size="large" />,
  );

  const [errorMessages, setErrorMessages] = useState([]);

  useEffect(() => {
    RNFS.readFile(ASSETS_FILE_PATH + 'ep-' + episode + '/script.json')
      .then((result) => {
        const script = JSON.parse(result);
        checkIntegrity(script, episode).then((tmpErrorMessages) => {
          setErrorMessages(tmpErrorMessages);
          setLoadMessage(null);
        });
      })
      .catch((err) => {
        setErrorMessages((curErrorMessages) => {
          curErrorMessages.push(
            'Error ' +
              err.code +
              ': ' +
              ASSETS_RELATIVE_PATH +
              'ep-' +
              episode +
              '/script.json ' +
              translate('missing'),
          );
          return curErrorMessages;
        });
        setLoadMessage(null);
      });
  }, [episode]);

  return (
    <Layout
      title={translate('episode') + ' ' + episode}
      goBack={() => goToEpisodeScreen()}>
      <Text>{translate('checkIntegrityDescription')}</Text>
      {loadMessage ? (
        <Text>{loadMessage}</Text>
      ) : (
        <>
          <Title>{translate('detectedErrors')}</Title>
          {!_.isEmpty(errorMessages) ? (
            <>
              <Text>{translate('detectedErrorsDescription')}</Text>
              {errorMessages.map((errorMessage, key) => (
                <Text key={'ep-' + episode + '-integrity-err-' - key}>
                  {errorMessage}
                </Text>
              ))}
            </>
          ) : (
            <Text>{translate('noErrorDetected')}</Text>
          )}
        </>
      )}
    </Layout>
  );
};

export default EpisodeIntegrityView;
