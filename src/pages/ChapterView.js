import React, {useEffect, useState} from 'react';
import Reader from '../components/Reader';
import {useHistory, useParams} from 'react-router-native';
import {BackHandler, Text} from 'react-native';
import RNFS from 'react-native-fs';
import Layout from '../components/Layout';
import {ActivityIndicator} from 'react-native-paper';
import {translate} from '../../utils/localize';
import {ASSETS_FILE_PATH, ASSETS_RELATIVE_PATH} from '../constants';

const ChapterView = () => {
  const params = useParams();

  const [pagerItems, setPagerItems] = useState(null);
  const [loadMessage, setLoadMessage] = useState(
    <ActivityIndicator size="large" />,
  );

  const episode = params.episode;
  const chapter = params.chapter;
  const [initialIndex, setInitialIndex] = useState(null);

  useEffect(() => {
    RNFS.readFile(ASSETS_FILE_PATH + 'ep-' + episode + '/script.json')
      .then((result) => {
        setLoadMessage(<ActivityIndicator size="large" />);

        const tmpScript = JSON.parse(result);
        let tmpPagerItems = [];
        let tmpIndex = null;
        Object.values(tmpScript.volumes).forEach((chapters) => {
          chapters.forEach((chapterIndex) => {
            const pages = tmpScript.chapters[chapterIndex].pages;
            if (chapter === chapterIndex && tmpIndex === null) {
              tmpIndex = tmpPagerItems.length;
            }
            for (let i = 0; i < pages.length; ++i) {
              let tmpPagerItem = pages[i];
              tmpPagerItem.chapter = chapterIndex;
              tmpPagerItem.bgm = tmpPagerItem.bgm
                ? ASSETS_FILE_PATH +
                  'common/bgm/umib_' +
                  tmpPagerItem.bgm +
                  '.ogg'
                : null;
              tmpPagerItem.se.forEach(function (se, seIndex, theArray) {
                // Do not switch to => function for "this" to work
                theArray[seIndex] =
                  ASSETS_FILE_PATH + 'common/se/umilse_' + se + '.ogg';
              });
              tmpPagerItem.voice = tmpPagerItem.voice
                ? ASSETS_FILE_PATH +
                  'ep-' +
                  episode +
                  '/voice/ch-' +
                  chapterIndex +
                  '/' +
                  tmpPagerItem.page +
                  '.ogg'
                : null;
              tmpPagerItem.uri =
                ASSETS_FILE_PATH +
                'ep-' +
                episode +
                '/img/ch-' +
                chapterIndex +
                '/' +
                pages[i].page +
                '.jpg';
              tmpPagerItems.push(tmpPagerItem);
            }
          });
        });
        setPagerItems(tmpPagerItems.slice().reverse());
        setInitialIndex(tmpPagerItems.length - tmpIndex - 1);
      })
      .catch((err) => {
        if (err.code === 'ENOENT') {
          setLoadMessage(
            translate('episode') +
              ' ' +
              episode +
              translate('notFoundIn') +
              ASSETS_RELATIVE_PATH +
              'ep-' +
              episode +
              '/. ' +
              translate('pleaseDownloadEpisode'),
          );
        } else {
          setLoadMessage(translate('errorLoadingScript') + episode);
        }
        setPagerItems(null);
        setInitialIndex(null);
        console.log(err.message, err.code);
      });
  }, [episode]);

  return pagerItems && initialIndex ? (
    <Reader
      pagerItems={pagerItems}
      episode={episode}
      initialIndex={initialIndex}
    />
  ) : (
    <Layout
      title={
        translate('episode') +
        ' ' +
        episode +
        ' ' +
        translate('chapter') +
        ' ' +
        chapter
      }
      goBack={() => goToEpisodeScreen()}>
      <Text>{loadMessage}</Text>
    </Layout>
  );
};

export default ChapterView;
