﻿import React, {useState} from 'react';
import Layout from '../components/Layout';
import EpisodeLink from '../components/EpisodeLink';
import {Platform, StyleSheet} from 'react-native';
import {Button, Dialog, Portal, Title} from 'react-native-paper';
import {translate} from '../../utils/localize';
import {ASSETS_FILE_PATH} from '../constants';
import RNFS from 'react-native-fs';
import {Link, useHistory} from 'react-router-native';

const s = StyleSheet.create({
  bold: {
    fontWeight: 'bold',
  },
  bottomMargin: {
    marginBottom: 10,
  },
  leftAlign: {
    textAlign: 'left',
  },
  link: {
    color: '#AA4336',
  },
});

const Homepage = () => {
  const history = useHistory();

  const [visible, setVisible] = useState(false);
  const showDialog = () => setVisible(true);
  const hideDialog = () => setVisible(false);

  // Init directory on iOS to be able to show the directory to the user in the Files app
  if (Platform.OS === 'ios') {
    RNFS.exists(ASSETS_FILE_PATH + '/ignore.txt')
      .then((ignoreExists) => {
        if (!ignoreExists) {
          RNFS.writeFile(ASSETS_FILE_PATH + '/ignore.txt', ' ', 'utf8')
            .then((result) => {
              // Do nothing
            })
            .catch((err) => {
              // Do nothing
            });
        }
      })
      .catch((ignoreErr) => {
        // Do nothing
      });
  }

  return (
    <Layout title={'Umineko no Naku Koro ni'} moreInfo={() => showDialog()}>
      <Title>{translate('episodes')}</Title>

      <EpisodeLink number={1} />
      <EpisodeLink number={2} />
      <EpisodeLink number={3} />
      <EpisodeLink number={4} />

      <EpisodeLink number={5} />
      <EpisodeLink number={6} />
      <EpisodeLink number={7} />
      <EpisodeLink number={8} />

      <EpisodeLink number={'tsubasa'} />
      <EpisodeLink number={'murasaki'} />

      <Portal>
        <Dialog visible={visible} onDismiss={hideDialog}>
          <Dialog.Title>Umineko Manga Reader</Dialog.Title>
          <Dialog.Content>
            <Button
              onPress={() => history.push('/installation')}
              style={s.bottomMargin}
              mode="outlined">
              {translate('installationInstructions')}
            </Button>

            <Button
              onPress={() => history.push('/about')}
              style={s.bottomMargin}
              mode="outlined">
              {translate('about')}
            </Button>
          </Dialog.Content>
          <Dialog.Actions>
            <Button onPress={hideDialog}>{translate('done')}</Button>
          </Dialog.Actions>
        </Dialog>
      </Portal>
    </Layout>
  );
};

export default Homepage;
