import {Platform} from 'react-native';
import {DocumentDirectoryPath} from 'react-native-fs';

export const ASSETS_RELATIVE_PATH = Platform.select({
  android: () => 'Android/obb/com.uminekomangareader/',
  ios: () => 'UminekoMangaReader/',
  windows: () => 'UminekoMangaReader/',
  default: () => 'UminekoMangaReader/',
})();
// file://
export const ASSETS_FILE_PATH = Platform.select({
  android: () => '/sdcard/' + ASSETS_RELATIVE_PATH,
  ios: () => DocumentDirectoryPath,
  windows: () => 'C:/' + ASSETS_RELATIVE_PATH,
  default: () => '/' + ASSETS_RELATIVE_PATH,
})();
