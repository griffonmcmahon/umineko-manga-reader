import EpisodeView from './pages/EpisodeView';
import Homepage from './pages/Homepage';
import ChapterView from './pages/ChapterView';
import EpisodeIntegrityView from './pages/EpisodeIntegrityView';
import About from './pages/About';
import Installation from './pages/Installation';

const routes = [
  {
    path: '/episode-:episode([1-8]|tsubasa|murasaki)',
    component: EpisodeView,
  },
  {
    path: '/episode-:episode([1-8]|tsubasa|murasaki)/integrity',
    component: EpisodeIntegrityView,
  },
  {
    path:
      '/episode-:episode([1-8]|tsubasa|murasaki)/chapter-:chapter([1-9]\\d*|interlude|confession-[1-3])',
    component: ChapterView,
  },
  {
    path: '/installation',
    component: Installation,
  },
  {
    path: '/about',
    component: About,
  },
  {
    path: '/',
    component: Homepage,
  },
];

export default routes;
