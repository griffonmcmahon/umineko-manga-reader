![](screenshots/mainScreen.png)
![](screenshots/episodeScreen.png)
![](screenshots/readerScreen.png)

# Description

Umineko Manga Reader is a comic reader mobile app made with React Native for the manga Umineko no Naku Koro ni.
This project is totally offline, works with Android, with planned support for iOS (developer needed), Windows and macOS.

It adds enhanced features during the reading such as:
- Background Music
- Sound Effects
- Dub (voices for dialogs)

Sound enhancement is based on the Visual Novel.
Scripts and assets are maintained here: https://github.com/papjul/umineko-reader-assets

The developer would like to thank Goldsmith user from YouTube who inspired him to make this project.


# Installation instructions
Prerequisities: At least 2 GB of storage (more is better).

- Download the app from the releases page (currently, only Android is available)
- Allow unknown sources to install the APK on your device
- The app itself contains no assets. You need to download them, all instructions to do this are available on the main screen.

Enjoy!

Note: Uninstalling the app will remove all assets from your Android internal storage, be careful!


# TODO

Bugs/Quality:
- P3 - Add ability to read assets directly from zip?
- P3 - Add ability to read assets from a specific folder? (for example, request permission to external storage)

Features:
- P2 - Improve UI/UX, make it more umineko-like
- P2 - Musicbox
- P2 - Dark theme
  
Platforms:
- P1 - Windows - Crash on start. Cannot find React Native Photo View. Looks like react-native.config.js is ignored. Find an audio library that is compatible with Windows and maintained (react-native-video?).
- P2 - iOS - Starts in simulator. Needs to be tested with packages unzipped. Needs all audios to be converted because Apple decided to support neither Vorbis or Opus audio codec. Help needed regarding the .IPA. Priority was lowered due to this platform being too jailed.
- P2 - MacOS - Work not started
- P3 - Linux with React Native Web?

Assets:
- P1 - Complete episodes 7 and 8
- P2 - More voices
- P2 - Complete Tsubasa
- P3 - More languages (help wanted)


# About

This project uses assets from https://github.com/papjul/umineko-reader-assets
